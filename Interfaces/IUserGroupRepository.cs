﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
	public interface IUserGroupRepository : IRepositoryBase<UserGroup>
	{
		Task CreateUG(UserGroup userGroup);
		Task DeleteUG(UserGroup userGroup);
	}
}
