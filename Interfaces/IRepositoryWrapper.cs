﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
	public interface IRepositoryWrapper
	{
		IUserRepository User { get; }
		IGroupRepository Group { get; }
		IUserGroupRepository UserGroup { get; }
		void Save();
		Task SaveAsync();
		void DetacheEntityState(object entity);
	}
}
