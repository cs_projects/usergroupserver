﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
	public interface IGroupRepository : IRepositoryBase<Group>
	{
		Task<List<Group>> GetAll();
		Task<Group> GetById(int id);
		Task CreateGroup(Group group);
		Task UpdateGroup(Group group);
		Task DeleteGroup(Group group);
	}
}
