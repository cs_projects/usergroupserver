﻿using Entities.DataTransferObjects;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
	public interface IUserRepository : IRepositoryBase<User>
	//public interface IUserRepository
	{
		Task<List<User>> GetAll();
		Task<User> GetById(int id);
		Task CreateUser(User user);
		Task UpdateUser(User user);
		Task DeleteUser(User user);
	}
}
