﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UserGroupServer.Migrations
{
	public partial class init : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "Groups",
				columns: table => new
				{
					Id = table.Column<int>(nullable: false)
						.Annotation("SqlServer:Identity", "1, 1"),
					Name = table.Column<string>(maxLength: 60, nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Groups", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "Users",
				columns: table => new
				{
					Id = table.Column<int>(nullable: false)
						.Annotation("SqlServer:Identity", "1, 1"),
					Name = table.Column<string>(maxLength: 60, nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Users", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "UserGroups",
				columns: table => new
				{
					UserId = table.Column<int>(nullable: false),
					GroupId = table.Column<int>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_UserGroups", x => new { x.UserId, x.GroupId });
					table.ForeignKey(
						name: "FK_UserGroups_Groups_GroupId",
						column: x => x.GroupId,
						principalTable: "Groups",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_UserGroups_Users_UserId",
						column: x => x.UserId,
						principalTable: "Users",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.InsertData(
				table: "Groups",
				columns: new[] { "Id", "Name" },
				values: new object[,]
				{
					{ 1, "admin" },
					{ 2, "users" },
					{ 3, "members" }
				});

			migrationBuilder.InsertData(
				table: "Users",
				columns: new[] { "Id", "Name" },
				values: new object[,]
				{
					{ 1, "Admin" },
					{ 2, "Volodya" },
					{ 3, "Seledkapod" },
					{ 4, "Igor" }
				});

			migrationBuilder.InsertData(
				table: "UserGroups",
				columns: new[] { "UserId", "GroupId" },
				values: new object[,]
				{
					{ 1, 1 },
					{ 1, 3 },
					{ 2, 2 },
					{ 3, 1 },
					{ 3, 3 },
					{ 4, 2 },
					{ 4, 3 }
				});

			migrationBuilder.CreateIndex(
				name: "IX_UserGroups_GroupId",
				table: "UserGroups",
				column: "GroupId");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "UserGroups");

			migrationBuilder.DropTable(
				name: "Groups");

			migrationBuilder.DropTable(
				name: "Users");
		}
	}
}
