﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace UserGroupServer.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class WeatherForecastController : ControllerBase
	{
		private IRepositoryWrapper _repoWrapper;

		public WeatherForecastController(IRepositoryWrapper repoWrapper)
		{
			_repoWrapper = repoWrapper;
		}

		[HttpGet]
		public IQueryable<Group> Get()
		{
			var adminGroups = _repoWrapper.Group.FindByCondition(x => x.Name.Equals("admin"));
			var users = _repoWrapper.User.GetAll();
			//return new string[] { "value1", "value2" };
			return adminGroups;
		}
	}
}
