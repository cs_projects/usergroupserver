﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;
using Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UserGroupServer.Controllers
{
	[Route("api/groups")]
	[ApiController]
	public class GroupController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;
		private IMapper _mapper;
		private string _message = "";

		public GroupController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
		{
			_logger = logger;
			_repository = repository;
			_mapper = mapper;
		}


		[HttpGet, ActionName("GetAll")]
		public async Task<ServiceResponse<List<GetGroupDto>>> GetAll()
		{
			ServiceResponse<List<GetGroupDto>> response = new ServiceResponse<List<GetGroupDto>>();

			try
			{
				var groups = await _repository.Group.GetAll();

				_message = $"Выбрали все группы из БД.";
				_logger.LogInfo(_message);
				response.Message = _message;

				var groupsResult = _mapper.Map<List<GetGroupDto>>(groups);
				for (int i = 0; i < groupsResult.Count; i++)
					groups[i].UserGroups.ForEach(g => groupsResult[i].Users.Add(_mapper.Map<GetUserGroupDto>(g.User)));

				response.Data = groupsResult;
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: GetAll, message: {ex.Message}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		[HttpGet("{id}"), ActionName("GetById")]
		public async Task<ServiceResponse<GetGroupDto>> GetById(int id)
		{
			ServiceResponse<GetGroupDto> response = new ServiceResponse<GetGroupDto>();

			try
			{
				var group = await _repository.Group.GetById(id);
				if (group == null)
				{
					_message = $"Группа с id: {id} не найдена.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}
				else
				{
					var groupResult = _mapper.Map<GetGroupDto>(group);
					group.UserGroups.ForEach(g => groupResult.Users.Add(_mapper.Map<GetUserGroupDto>(g.User)));

					_message = $"Была найдена группа с id: {id}.";
					_logger.LogInfo(_message);
					response.Message = _message;

					response.Data = groupResult;
				}
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: GetById(int id: {id}), message: {ex.Message}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		// Создает группу
		// Формат запроса:
		//	{
		//		"name": "Name", // Required
		//	}
		[HttpPost, ActionName("CreateGroup")]
		public async Task<ServiceResponse<GetGroupDto>> CreateGroup([FromBody] AddGroupDto group)
		{
			ServiceResponse<GetGroupDto> response = new ServiceResponse<GetGroupDto>();

			try
			{
				if (group == null || String.IsNullOrWhiteSpace(group.Name))
				{
					_message = $"Отправленная группа is null.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}
				if (!ModelState.IsValid)
				{
					_message = $"Неверная модель группы.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				var groupEntity = _mapper.Map<Group>(group);

				await _repository.Group.CreateGroup(groupEntity);
				await _repository.SaveAsync();
				_repository.DetacheEntityState(groupEntity);

				response = await GetById(groupEntity.Id);
				response.Message = $"Была добавлена группа с id: {response.Data.Id}";
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: CreateGroup([FromBody] AddGroupDto group: {group}), message: {ex}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		[HttpPost("{name}"), ActionName("NotAllowedPost")]
		public ServiceResponse<IActionResult> NotAllowedPost(string name)
		{
			ServiceResponse<IActionResult> response = new ServiceResponse<IActionResult>();

			_message = $"Данный метод не поддерживается HttpPost(name: {name}, action = NotAllowedPost)";
			_logger.LogError(_message);
			response.Success = false;
			response.Message = _message;

			return response;
		}

		// Обновляет группу
		// Формат запроса:
		//	{
		//		"name": "Name", // Required
		//	}
		[HttpPut("{id}"), ActionName("UpdateGroup")]
		public async Task<ServiceResponse<GetGroupDto>> UpdateGroup(int id, [FromBody] AddGroupDto group)
		{
			ServiceResponse<GetGroupDto> response = new ServiceResponse<GetGroupDto>();

			try
			{
				if (group == null || String.IsNullOrWhiteSpace(group.Name))
				{
					_message = $"Отправленная группа is null.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}
				if (!ModelState.IsValid)
				{
					_message = $"Неверная модель группы.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				var groupEntity = await _repository.Group.GetById(id);

				if (groupEntity == null)
				{
					_message = $"Группа с id: {id} не найдена.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				_mapper.Map(group, groupEntity);

				await _repository.Group.UpdateGroup(groupEntity);
				await _repository.SaveAsync();
				_repository.DetacheEntityState(groupEntity);

				response = await GetById(groupEntity.Id);
				response.Message = $"Была обновлена группа с id: {response.Data.Id}";
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: UpdateGroup([FromBody] AddUserDto group: {group}), message: {ex}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}


		[HttpDelete("{id}"), ActionName("DeleteGroup")]
		public async Task<ServiceResponse<GetGroupDto>> DeleteGroup(int id)
		{
			ServiceResponse<GetGroupDto> response = new ServiceResponse<GetGroupDto>();

			try
			{
				var group = await _repository.Group.GetById(id);
				if (group == null)
				{
					_message = $"Группа с id: {id} не найден.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				await _repository.Group.DeleteGroup(group);
				await _repository.SaveAsync();
				response.Message = $"Была удалена группа с id: {id}";
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: DeleteGroup(int id: {id}), message: {ex}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		[HttpDelete, ActionName("NotAllowedDelete")]
		public ServiceResponse<IActionResult> NotAllowedDelete()
		{
			ServiceResponse<IActionResult> response = new ServiceResponse<IActionResult>();

			_message = $"Данный метод не поддерживается, передайте id группы для удаления HttpDelete(action = NotAllowedDelete)";
			_logger.LogError(_message);
			response.Success = false;
			response.Message = _message;

			return response;
		}
	}
}
