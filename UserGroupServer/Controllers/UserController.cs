﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Interfaces;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Entities;

namespace UserGroupServer.Controllers
{
	[Route("api/users")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private ILoggerManager _logger;
		private IRepositoryWrapper _repository;
		private IMapper _mapper;
		private string _message = "";

		public UserController(ILoggerManager logger, IRepositoryWrapper repository, IMapper mapper)
		{
			_logger = logger;
			_repository = repository;
			_mapper = mapper;
		}

		[HttpGet, ActionName("GetAll")]
		public async Task<ServiceResponse<List<GetUserDto>>> GetAll()
		{
			ServiceResponse<List<GetUserDto>> response = new ServiceResponse<List<GetUserDto>>();

			try
			{
				var users = await _repository.User.GetAll();

				_message = $"Выбрали всех пользователей из БД.";
				_logger.LogInfo(_message);
				response.Message = _message;

				var usersResult = _mapper.Map<List<GetUserDto>>(users);
				for (int i = 0; i < usersResult.Count; i++)
					users[i].UserGroups.ForEach(g => usersResult[i].Groups.Add(_mapper.Map<GetUserGroupDto>(g.Group)));

				response.Data = usersResult;
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: GetAll, message: {ex.Message}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		[HttpGet("{id}"), ActionName("GetById")]
		public async Task<ServiceResponse<GetUserDto>> GetById(int id)
		{
			ServiceResponse<GetUserDto> response = new ServiceResponse<GetUserDto>();

			try
			{
				var user = await _repository.User.GetById(id);
				if (user == null)
				{
					_message = $"Пользователь с id: {id} не найден.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}
				else
				{
					var userResult = _mapper.Map<GetUserDto>(user);
					user.UserGroups.ForEach(g => userResult.Groups.Add(_mapper.Map<GetUserGroupDto>(g.Group)));

					_message = $"Был найден пользователь с id: {id}.";
					_logger.LogInfo(_message);
					response.Message = _message;

					response.Data = userResult;
				}
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: GetById(int id: {id}), message: {ex.Message}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		// Создает пользователя с привязанными к нему группами
		// Формат запроса:
		//	{
		//		"name": "Name", // Required
		//		"userGroups":
		//		[
		//			{
		//				"groupId": 1
		//			},
		//			{
		//				"groupId": 2
		//			},
		//			{
		//				"groupId": 3
		//			}
		//		]
		//	}
		[HttpPost, ActionName("CreateUser")]
		public async Task<ServiceResponse<GetUserDto>> CreateUser([FromBody] AddUserDto user)
		{
			ServiceResponse<GetUserDto> response = new ServiceResponse<GetUserDto>();

			try
			{
				if (user == null || String.IsNullOrWhiteSpace(user.Name))
				{
					_message = $"Отправленный пользователь is null.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}
				if (!ModelState.IsValid)
				{
					_message = $"Неверная модель пользователя.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				var userEntity = _mapper.Map<User>(user);

				await _repository.User.CreateUser(userEntity);
				await _repository.SaveAsync();
				_repository.DetacheEntityState(userEntity);

				response = await GetById(userEntity.Id);
				response.Message = $"Был добавлен пользователь с id: {response.Data.Id}";
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: CreateUser([FromBody] AddUserDto user: {user}), message: {ex}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		[HttpPost("{name}"), ActionName("NotAllowedPost")]
		public ServiceResponse<IActionResult> NotAllowedPost(string name)
		{
			ServiceResponse<IActionResult> response = new ServiceResponse<IActionResult>();

			_message = $"Данный метод не поддерживается HttpPost(name: {name}, action = NotAllowedPost)";
			_logger.LogError(_message);
			response.Success = false;
			response.Message = _message;

			return response;
		}

		// Обновляет пользователя с привязанными к нему группами
		// Формат запроса:
		//	{
		//		"name": "Name", // Required
		//		"userGroups":
		//		[
		//			{
		//				"groupId": 1
		//			},
		//			{
		//				"groupId": 2
		//			},
		//			{
		//				"groupId": 3
		//			}
		//		]
		//	}
		[HttpPut("{id}"), ActionName("UpdateUser")]
		public async Task<ServiceResponse<GetUserDto>> UpdateUser(int id, [FromBody] UpdateUserDto user)
		{
			ServiceResponse<GetUserDto> response = new ServiceResponse<GetUserDto>();

			try
			{
				if (user == null || String.IsNullOrWhiteSpace(user.Name))
				{
					_message = $"Отправленный пользователь is null.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}
				if (!ModelState.IsValid)
				{
					_message = $"Неверная модель пользователя.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				if (user.UserGroups != null)
					user.UserGroups.ForEach(u => u.UserId = id);

				var userEntity = await _repository.User.GetById(id);

				if (userEntity == null)
				{
					_message = $"Пользователь с id: {id} не найден.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				var userGroupEntity = _mapper.Map<List<UserGroup>>(user.UserGroups);

				if (await TryUpdateModelAsync<User>(userEntity, "", i => i.Name, i => i.UserGroups))
				{
					await UpdateUserGroups(userGroupEntity, userEntity);
					try
					{
						await _repository.SaveAsync();
						_repository.DetacheEntityState(userEntity);
					}
					catch (DbUpdateException /* ex */)
					{
						//Log the error (uncomment ex variable name and write a log.)
						ModelState.AddModelError("", "Unable to save changes. " +
							"Try again, and if the problem persists, " +
							"see your system administrator.");
					}
				}

				_mapper.Map(user, userEntity);

				//userEntity.UserGroups = userGroupEntity;
				//userEntity.UserGroups = new List<UserGroup>();

				await _repository.User.UpdateUser(userEntity);
				await _repository.SaveAsync();
				_repository.DetacheEntityState(userEntity);

				//await UpdateUserGroups(userGroupEntity, userEntity);

				response = await GetById(userEntity.Id);
				response.Message = $"Был обновлен пользователь с id: {response.Data.Id}";
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: UpdateUser([FromBody] AddUserDto user: {user}), message: {ex}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		private async Task UpdateUserGroups(List<UserGroup> userGroupEntity, User userEntity)
		{
			if (userGroupEntity == null)
			{
				userEntity.UserGroups = new List<UserGroup>();
				return;
			}

			var selectedGroupsIds = new HashSet<int>(userGroupEntity.Select(u => u.GroupId));
			//userGroupEntity.ForEach(i => selectedGroupsIds.Add(i.GroupId));

			var userGroupsIds = new HashSet<int>(userEntity.UserGroups.Select(u => u.GroupId));

			foreach (var group in _repository.Group.FindAll())
			{
				if (selectedGroupsIds.Contains(group.Id))
				{
					if (!userGroupsIds.Contains(group.Id))
					{
						UserGroup entity = new UserGroup { UserId = userEntity.Id, GroupId = group.Id };
						await _repository.UserGroup.CreateUG(entity);
						await _repository.SaveAsync();
						_repository.DetacheEntityState(entity);

					}
				}
				else
				{
					if (userGroupsIds.Contains(group.Id))
					{
						UserGroup userGroupToRemove = userEntity.UserGroups.FirstOrDefault(i => i.GroupId == group.Id);
						await _repository.UserGroup.DeleteUG(userGroupToRemove);
						await _repository.SaveAsync();
					}
				}
			}

		}

		[HttpDelete("{id}"), ActionName("DeleteUser")]
		public async Task<ServiceResponse<GetUserDto>> DeleteUser(int id)
		{
			ServiceResponse<GetUserDto> response = new ServiceResponse<GetUserDto>();

			try
			{
				var user = await _repository.User.GetById(id);
				if (user == null)
				{
					_message = $"Пользователь с id: {id} не найден.";
					_logger.LogError(_message);
					response.Success = false;
					response.Message = _message;

					return response;
				}

				await _repository.User.DeleteUser(user);
				await _repository.SaveAsync();
				response.Message = $"Был удален пользователь с id: {id}";
			}
			catch (Exception ex)
			{
				_message = $"Что-то пошло не так, action: DeleteUser(int id: {id}), message: {ex}.";
				_logger.LogError(_message);
				response.Success = false;
				response.Message = _message;
			}

			return response;
		}

		[HttpDelete, ActionName("NotAllowedDelete")]
		public ServiceResponse<IActionResult> NotAllowedDelete()
		{
			ServiceResponse<IActionResult> response = new ServiceResponse<IActionResult>();

			_message = $"Данный метод не поддерживается, передайте id пользователя для удаления HttpDelete(action = NotAllowedDelete)";
			_logger.LogError(_message);
			response.Success = false;
			response.Message = _message;

			return response;
		}
	}
}
