﻿using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserGroupServer
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<User, GetUserDto>();
			CreateMap<Group, GetUserGroupDto>();

			CreateMap<Group, GetGroupDto>();
			CreateMap<User, GetUserGroupDto>();

			CreateMap<AddUserDto, User>();
			CreateMap<AddUserGroupDto, UserGroup>();

			CreateMap<AddGroupDto, Group>();

			CreateMap<UpdateUserDto, User>();
			CreateMap<UpdateUserGroupDto, UserGroup>();
		}
	}
}
