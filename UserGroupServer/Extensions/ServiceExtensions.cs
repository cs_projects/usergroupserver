﻿using Interfaces;
using Entities;
using LoggerService;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using System;
using System.Reflection;

namespace UserGroupServer.Extensions
{
	public static class ServiceExtensions
	{
		public static void ConfigureCors(this IServiceCollection services)
		{
			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					builder => builder.AllowAnyOrigin()
					.AllowAnyMethod()
					.AllowAnyHeader());
			});
		}

		public static void ConfigureIISIntegration(this IServiceCollection services)
		{
			services.Configure<IISOptions>(options =>
			{
			});
		}

		public static void ConfigureLoggerService(this IServiceCollection services)
		{
			services.AddSingleton<ILoggerManager, LoggerManager>();
		}

		public static void ConfigureMsSqlContext(this IServiceCollection services, IConfiguration config)
		{
			var connectionString = config["mssqlconnection:ConnectionString"];
			services.AddDbContext<RepositoryContext>(options =>
			{
				options.UseSqlServer(connectionString,
				sqlServerOptionsAction: sqlOptions =>
				{
					sqlOptions.MigrationsAssembly(
						typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
					sqlOptions.
						EnableRetryOnFailure(maxRetryCount: 5,
						maxRetryDelay: TimeSpan.FromSeconds(30),
						errorNumbersToAdd: null);
				});

				options.ConfigureWarnings(warnings => warnings.Throw(
					RelationalEventId.BoolWithDefaultWarning));

				//options.ConfigureWarnings(warnings => warnings.Log((
				//	RelationalEventId.CommandExecuting, LogLevel.Debug)));
			});
		}

		public static void ConfigureRepositoryWrapper(this IServiceCollection services)
		{
			services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
		}
	}
}
