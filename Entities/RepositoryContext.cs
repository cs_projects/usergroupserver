﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
	public class RepositoryContext : DbContext
	{
		public RepositoryContext(DbContextOptions<RepositoryContext> options)
			: base(options)
		{
			Database.EnsureCreated();
		}

		public DbSet<User> Users { get; set; }
		public DbSet<Group> Groups { get; set; }
		public DbSet<UserGroup> UserGroups { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<User>().HasData(new User { Id = 1, Name = "Admin" });
			modelBuilder.Entity<User>().HasData(new User { Id = 2, Name = "Volodya" });
			modelBuilder.Entity<User>().HasData(new User { Id = 3, Name = "Seledkapod" });
			modelBuilder.Entity<User>().HasData(new User { Id = 4, Name = "Igor" });

			modelBuilder.Entity<Group>().HasData(new Group { Id = 1, Name = "admin" });
			modelBuilder.Entity<Group>().HasData(new Group { Id = 2, Name = "users" });
			modelBuilder.Entity<Group>().HasData(new Group { Id = 3, Name = "members" });

			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 1, GroupId = 1 });
			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 1, GroupId = 3 });
			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 2, GroupId = 2 });
			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 3, GroupId = 1 });
			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 3, GroupId = 3 });
			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 4, GroupId = 2 });
			modelBuilder.Entity<UserGroup>().HasData(new UserGroup { UserId = 4, GroupId = 3 });

			modelBuilder.Entity<UserGroup>().HasKey(sc => new { sc.UserId, sc.GroupId });
		}
	}
}
