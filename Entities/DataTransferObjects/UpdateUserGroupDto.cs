﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class UpdateUserGroupDto
	{
		[Required(ErrorMessage = "UserId обязательное поле")]
		public int UserId { get; set; }
		[Required(ErrorMessage = "GroupId обязательное поле")]
		public int GroupId { get; set; }
	}
}
