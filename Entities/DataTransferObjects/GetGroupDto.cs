﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class GetGroupDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public List<GetUserGroupDto> Users { get; set; }
	}
}
