﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class GetUserGroupDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
