﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class AddGroupDto
	{
		[Required(ErrorMessage = "Название группы обязательное поле")]
		[StringLength(60, ErrorMessage = "Название группы не может быть длиннее 60 символов")]
		public string Name { get; set; }
	}
}
