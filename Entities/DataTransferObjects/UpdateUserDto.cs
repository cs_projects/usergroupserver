﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class UpdateUserDto
	{
		[Required(ErrorMessage = "Имя пользователя обязательное поле")]
		[StringLength(60, ErrorMessage = "Имя пользователя не может быть длиннее 60 символов")]
		public string Name { get; set; }
		//[Required(ErrorMessage = "Группы пользователя обязательны")]
		public List<UpdateUserGroupDto> UserGroups { get; set; }
	}
}
