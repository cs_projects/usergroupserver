﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class AddUserGroupDto
	{
		public int UserId { get; set; }
		public int GroupId { get; set; }
	}
}
