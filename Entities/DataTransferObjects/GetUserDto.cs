﻿using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.DataTransferObjects
{
	public class GetUserDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public List<GetUserGroupDto> Groups { get; set; }
	}
}
