﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
	public class Group
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "Название группы обязательное поле")]
		[StringLength(60, ErrorMessage = "Название группы не может быть длиннее 60 символов")]
		public string Name { get; set; }

		[NotMapped]
		public List<User> Users { get; set; }
		public List<UserGroup> UserGroups { get; set; }
	}
}
