﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
	public class User
	{
		public int Id { get; set; }

		[Required(ErrorMessage = "Имя пользователя обязательное поле")]
		[StringLength(60, ErrorMessage = "Имя пользователя не может быть длиннее 60 символов")]
		public string Name { get; set; }

		[NotMapped]
		public List<Group> Groups { get; set; }
		public List<UserGroup> UserGroups { get; set; }
	}
}
