﻿using Entities.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entities
{
	public static class DbInitializer
	{
		public static void Initialize(RepositoryContext context)
		{
			//context.Database.EnsureCreated();

			// Look for any students.
			if (context.Users.Any())
			{
				return;   // DB has been seeded
			}

			var users = new User[]
			{
				new User { Id = 1, Name = "Admin" },
				new User { Id = 2, Name = "Volodya" },
				new User { Id = 3, Name = "Seledkapod" },
				new User { Id = 4, Name = "Igor" }
			};

			foreach (User u in users)
			{
				context.Users.Add(u);
			}
			context.SaveChanges();

			var groups = new Group[]
			{
				new Group { Id = 1, Name = "admin" },
				new Group { Id = 2, Name = "users" },
				new Group { Id = 3, Name = "members" },
			};

			foreach (Group g in groups)
			{
				context.Groups.Add(g);
			}
			context.SaveChanges();

			var usergroups = new UserGroup[]
			{
				new UserGroup { UserId = 1, GroupId = 1 },
				new UserGroup { UserId = 1, GroupId = 3 },
				new UserGroup { UserId = 2, GroupId = 2 },
				new UserGroup { UserId = 3, GroupId = 1 },
				new UserGroup { UserId = 3, GroupId = 3 },
				new UserGroup { UserId = 4, GroupId = 2 },
				new UserGroup { UserId = 4, GroupId = 3 },
			};

			foreach (UserGroup ug in usergroups)
			{
				var usergroupInDataBase = context.UserGroups.Where(
					s =>
						s.User.Id == ug.UserId &&
						s.Group.Id == ug.GroupId).SingleOrDefault();
				if (usergroupInDataBase == null)
				{
					context.UserGroups.Add(ug);
				}
			}
			context.SaveChanges();

		}
	}
}
