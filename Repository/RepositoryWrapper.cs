﻿using Interfaces;
using Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Repository
{
	public class RepositoryWrapper : IRepositoryWrapper
	{
		private RepositoryContext _repoContext;
		private IUserRepository _user;
		private IGroupRepository _group;
		private IUserGroupRepository _usergroup;
		public IUserRepository User
		{
			get
			{
				if (_user == null)
				{
					_user = new UserRepository(_repoContext);
				}
				return _user;
			}
		}
		public IGroupRepository Group
		{
			get
			{
				if (_group == null)
				{
					_group = new GroupRepository(_repoContext);
				}
				return _group;
			}
		}
		public IUserGroupRepository UserGroup
		{
			get
			{
				if (_usergroup == null)
				{
					_usergroup = new UserGroupRepository(_repoContext);
				}
				return _usergroup;
			}
		}
		public RepositoryWrapper(RepositoryContext repositoryContext)
		{
			_repoContext = repositoryContext;
		}
		public void Save()
		{
			_repoContext.SaveChanges();
		}
		public async Task SaveAsync()
		{
			await _repoContext.SaveChangesAsync();
		}
		public void DetacheEntityState(object entity)
		{
			_repoContext.Entry(entity).State = EntityState.Detached;
		}
	}
}