﻿using Interfaces;
using Entities;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Repository
{
	class GroupRepository : RepositoryBase<Group>, IGroupRepository
	{
		public GroupRepository(RepositoryContext repositoryContext)
			: base(repositoryContext)
		{
		}

		public async Task<List<Group>> GetAll()
		{
			return await FindAll()
				.Include(u => u.UserGroups)
					.ThenInclude(u => u.User)
				.OrderBy(u => u.Id)
				.ToListAsync();
		}

		public async Task<Group> GetById(int id)
		{
			return await FindByCondition(u => u.Id.Equals(id))
				.Include(u => u.UserGroups)
					.ThenInclude(u => u.User)
				.FirstOrDefaultAsync();
		}

		public async Task CreateGroup(Group group)
		{
			await Create(group);
		}

		public async Task UpdateGroup(Group group)
		{
			await Update(group);
		}

		public async Task DeleteGroup(Group group)
		{
			await Delete(group);
		}
	}
}
