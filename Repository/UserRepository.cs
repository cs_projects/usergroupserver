﻿using Interfaces;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.DataTransferObjects;

namespace Repository
{
	public class UserRepository : RepositoryBase<User>, IUserRepository
	{
		public UserRepository(RepositoryContext repositoryContext)
			: base(repositoryContext)
		{
		}

		public async Task<List<User>> GetAll()
		{
			return await FindAll()
				.Include(u => u.UserGroups)
					.ThenInclude(u => u.Group)
				.OrderBy(u => u.Id)
				.ToListAsync();
		}

		public async Task<User> GetById(int id)
		{
			return await FindByCondition(u => u.Id.Equals(id))
				.Include(u => u.UserGroups)
					.ThenInclude(u => u.Group)
				.FirstOrDefaultAsync();
		}

		public async Task CreateUser(User user)
		{
			 await Create(user);
		}

		public async Task UpdateUser(User user)
		{
			 await Update(user);
		}

		public async Task DeleteUser(User user)
		{
			 await Delete(user);
		}
	}
}
