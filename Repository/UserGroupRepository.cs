﻿using Entities;
using Entities.Models;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
	public class UserGroupRepository : RepositoryBase<UserGroup>, IUserGroupRepository
	{
		public UserGroupRepository(RepositoryContext repositoryContext)
			: base(repositoryContext)
		{
		}

		public async Task CreateUG(UserGroup userGroup)
		{
			await Create(userGroup);
		}

		public async Task DeleteUG(UserGroup userGroup)
		{
			await Delete(userGroup);
		}
	}
}
