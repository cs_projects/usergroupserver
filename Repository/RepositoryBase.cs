﻿using Interfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repository
{
	public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
	{
		protected RepositoryContext RepositoryContext { get; set; }
		public RepositoryBase(RepositoryContext repositoryContext)
		{
			this.RepositoryContext = repositoryContext;
		}
		public IQueryable<T> FindAll()
		{
			return this.RepositoryContext.Set<T>().AsNoTracking();
		}
		public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
		{
			return this.RepositoryContext.Set<T>().Where(expression).AsNoTracking();
		}
		public async Task Create(T entity)
		{
			await this.RepositoryContext.Set<T>().AddAsync(entity);
		}
		public async Task Update(T entity)
		{
			this.RepositoryContext.Set<T>().Update(entity);
		}
		public async Task Delete(T entity)
		{
			//this.RepositoryContext.Set<T>().Remove(entity);
			this.RepositoryContext.Entry(entity).State = EntityState.Deleted;
		}
	}
}
